# Instituto Superior de Engenharia da Universidade do Algarve

[![N|Solid](http://ise.ualg.pt/sites/default/files/theme-logos/ise.svg)](https://ise.ualg.pt)

## Projeto Ambientes de Desenvolvimento e Colaboração

## Instruções de Instalação
Para correr a aplicação é necessário instalar o módulo tabulate.
##
No terminal escreva:
###
```sh
   pip install tabulate
```

## Funcionalidades

- Adicionar Trotineta
- Adicionar Utilizador
- Adicionar Aluguer
- Listar Trotinetas
- Listar Utilizadores
- Listar Alugueres
- Guardar dados de utilizador e de trotinetas
- Carregar dados de utilizador e  de trotinetas

## Menu Inicial
###
```sh
    *********************************************************************
    :        A trote - Sistema de gestão de aluguer de trotinetas       : 
    *********************************************************************
    :                                                                   :
    : tn - nova trotineta       tl - lista trotinetas                   :
    : un - novo utilizador      ul - lista utilizadores                 :
    : an - novo aluguer         al - lista alugueres                    :
    :                                                                   :
    : g - guarda tudo           c - carrega tudo                        :
    :                                                                   :
    : x - sair                                                          :
    :                                                                   :
    *********************************************************************
```

## Nova Trotineta - TN
![Inserir Troti.png](https://bitbucket.org/repo/y5xpdA9/images/1990438169-Inserir%20Troti.png)
## Novo Utilizador - UN
![Inserir utilizador.png](https://bitbucket.org/repo/y5xpdA9/images/597793701-Inserir%20utilizador.png)
## Novo Aluguer - AN
![Novo aluguer (1).png](https://bitbucket.org/repo/y5xpdA9/images/1564668019-Novo%20aluguer%20%281%29.png)
## Listar Trotinetas - TL
![TL.png](https://bitbucket.org/repo/y5xpdA9/images/999669427-TL.png)
## Listar Trotinetas - UL
![ul.png](https://bitbucket.org/repo/y5xpdA9/images/1522267491-ul.png)
## Carrega Tudo - AL
![al.png](https://bitbucket.org/repo/y5xpdA9/images/2425113723-al.png)
## Guarda Tudo - G
![g.png](https://bitbucket.org/repo/y5xpdA9/images/1941239137-g.png)
## Carrega Tudo - C
![c.png](https://bitbucket.org/repo/y5xpdA9/images/2482351104-c.png)


## Ficheiros

- [main](main)
- [io_terminal](io_terminal)
- [io_ficheiros](io_ficheiros)
- [trotinetas](trotinetas)
- [utilizadores](utilizadores)
- [aluguer](aluguer)
- [main.html](doc/main.html)
- [io_terminal.html](doc/io_terminal)
- [io_ficheiros.html](doc/io_ficheiros)
- [trotinetas.html](doc/trotinetas)
- [utilizadores.html](doc/utilizadores)
- [aluguer.html](doc/aluguer)